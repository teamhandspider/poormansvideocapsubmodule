﻿using UnityEngine;

public class RotateInVideoTest : MonoBehaviour 
{
	public float RotateSpeed = 0.1f;

	void Update() 
	{
		transform.Rotate (RotateSpeed * Time.deltaTime * 60f, 0f, 0f,Space.Self);
	}
}
