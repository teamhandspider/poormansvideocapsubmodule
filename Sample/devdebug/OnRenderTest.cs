﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnRenderTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public bool doBlit = true;

    public RenderTexture testSrc;
    public RenderTexture testDst;
    public Vector2 renderTexScale;
    public Vector2 blitScale;
    public Vector2 blitOffset;

    public RenderTexture testOutputRendTex = null;

    void OnRenderImage(RenderTexture src, RenderTexture dest) {

        testSrc = src;
        testDst = dest;
        
        if (doBlit) Graphics.Blit(src, dest, blitScale, blitOffset);

        if(testOutputRendTex == null || testOutputRendTex.width != src.width * renderTexScale.x || testOutputRendTex.height != src.height * renderTexScale.y) {
            testOutputRendTex = new RenderTexture( (int)(src.width * renderTexScale.x), (int)(src.height * renderTexScale.y), 16);
        }
        Graphics.Blit(src, testOutputRendTex, blitScale, blitOffset);

        GameObject.Find("ubee").GetComponentInChildren<Renderer>().material.mainTexture = testOutputRendTex;
    }
}
