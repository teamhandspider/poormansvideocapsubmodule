﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoorMansVideoCaptureUsageSample : MonoBehaviour {

    public Button captureButton;
    public Text infoText;
    public Text resultInfoText;

    private void Start() {
        captureButton.onClick.AddListener(ClickedCapture);
    }

    // Update is called once per frame
    void Update () {
        captureButton.gameObject.SetActive(PoorMansVideoCapture.instance.IsCurrentlyCapturing);

        if(PoorMansVideoCapture.instance.IsCurrentlyCapturing) {
            infoText.text = "Capturing flashback. Press Capture to save it.";
        }
        else if(PoorMansVideoCapture.instance.processingInProgress) {
            infoText.text = "Saving flashback. Please wait.";
        }
        else {
            infoText.text = "Not ready";
        }
	}

    private void ClickedCapture() {
        resultInfoText.text = "";
        PoorMansVideoCapture.instance.SaveFlashbackRecording(OnVideoWasSaved);
    }

    private void OnVideoWasSaved(string capturedToPath, bool success) {
        if(success) {
            resultInfoText.text = "Flashback was captured to:\n" + capturedToPath;
        }
        else {
            resultInfoText.text = "Last capture failed!";
        }
    }
}
