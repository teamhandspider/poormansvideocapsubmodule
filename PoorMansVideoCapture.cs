﻿//#define USE_EXTERNAL_IMAGESAVING //uses System.Drawing and stuff to make image saving process faster (off main thread) (NOTE:might actullay not work at all in builds. Disabled by default)

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

#if USE_EXTERNAL_IMAGESAVING
using System.Drawing;
#endif

public class PoorMansVideoCapture : MonoBehaviour {

    [System.Serializable]
    public class CapturedFrame : System.IDisposable {
        public int width;
        public int height;
        [System.NonSerialized]public byte[] rawData;
        public Texture2D retainedTexture = null;
        public RenderTexture retainedRenderTex = null;

        public void Dispose() {
            print("disposing");
            if(retainedTexture != null) {
                PoorMansVideoCapture.instance.GiveTexToPool(retainedTexture);
            }
            if (retainedTexture != null) {
                PoorMansVideoCapture.instance.GiveTexToRenderTexPool(retainedRenderTex);
            }
        }
    }


    public static PoorMansVideoCapture instance;
    
	// Use this for initialization
	void Start () {

        if(instance != null) {
            Debug.LogError("Many instances of PoorMansVideoCapture - this is not supported!");
#if UNITY_EDITOR
            UnityEditor.EditorUtility.DisplayDialog("PoorMansVideoCapture", "Many instances of PoorMansVideoCapture - this is not supported!", "ok");
#endif
            DestroyImmediate(this);
            return;
        }

        timeSpentStopWatch = System.Diagnostics.Stopwatch.StartNew();

        if (Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.WindowsPlayer) {
            Log("Not windows - destroying self");
            DestroyImmediate(gameObject);
            return;
        }

        if(System.IO.File.Exists(Application.streamingAssetsPath+"/disablevideocap")) {
            Log("Have disablevideocap file - destroying self");
            DestroyImmediate(gameObject);
            return;
        }


        instance = this;


        showPassiveStats = PlayerPrefs.GetInt("PoorMansVideoPassiveUIVisible",debugUiOnByDefault ? 1 : 0) == 1;

        applicationDotTemporaryCachePath = Application.temporaryCachePath;
        applicationDotPersistentDataPath = Application.persistentDataPath;

        var ffmpegRootPath = "";
        /*if (Application.isEditor) {
            ffmpegRootPath = Application.
        }
        else*/ ffmpegRootPath = Application.dataPath;

        ffmpegPath = ffmpegRootPath + "/PoorMansVideoCapture/ffmpeg.exe";
               

        try { 
            //delete prev data at start instead of at exit to allow for delayed processing of videos
            var tempdir = new DirectoryInfo(applicationDotTemporaryCachePath + "/PoorMansVideoCaptureTemp");
            if (tempdir.Exists) {
                Log("deleting temp folder to get rid of last run stuff at " + tempdir.FullName);
                tempdir.Delete(true);
            }
        }
        catch(System.Exception e) {
            Log("Could not delete temp folder, oh well "+e);
        }

        StartCoroutine(CapturingRoutine());
	}

    public bool capturingOn = true;
    public float targetFPS = 15f;
    public int targetApproxVideoWidth = 480;
    public float flashbackCaptureSecondsCount = 30;

    public bool debugUiOnByDefault = false;

    [Header("If this is over 0, this number will be used instead of the normal fps number.")]
    public float overrideFps = -1f;

    [Space(50)]
    [Header("Advanced settings / debug")]
    public float lastCapturedAtTime = -100f;

    public List<CapturedFrame> capturedFramesBuffer = new List<CapturedFrame>();

    public int rtAntiAliasingAmount = 1;

    public bool IsCurrentlyCapturing {
        get {
            return
                capturingOn && haveFFMPEG;
        }
    }

    private IEnumerator CapturingRoutine()
    {
        yield return StartCoroutine(EnsureHaveFFMPEG());
        if(ffmpegDLFailed || !haveFFMPEG) {
            Log("well I guess this isnt going to work",true);
            yield break;
        }

        Log("Starting operation");

        var waitforendofframe = new WaitForEndOfFrame();

        while (true) {
            yield return waitforendofframe;

            if (!capturingOn) continue;

            var timeSinceLastCaptured = Time.realtimeSinceStartup - lastCapturedAtTime;

            var fpsToUse = overrideFps > 0f ? overrideFps : targetFPS;

            var timeToNextCapture = (1f / fpsToUse) - timeSinceLastCaptured;
            timeToNextCapture -= (Time.unscaledDeltaTime * 0.5f);

            if (timeToNextCapture < 0f) {
                //CaptureOne();
                StartCoroutine(CaptureOne());
                lastCapturedAtTime = Time.realtimeSinceStartup;
            }

            var secFloored = Mathf.FloorToInt(Time.realtimeSinceStartup);
            if(secFloored != capturesCounterForSecond) {
                capturesLastSecond = capturesThisSecCounter;
                capturesThisSecCounter = 0;
                capturesCounterForSecond = secFloored;
            }
        }
    }
        

    string directFFMPEGDownloadUrl = "https://randomdlsbackend20190427101716.azurewebsites.net/api/values/dlfile/ffmpeg.exe";

    bool downloadingFFMPEG = false;

    private IEnumerator EnsureHaveFFMPEG() {
        yield return null;

        if(File.Exists(ffmpegPath)) {
            haveFFMPEG = true;
            Log("have ffmpeg, no worries");
        }
        else {
            downloadingFFMPEG = true;
            Log("DONT HAVE FFMPEG, downloading it now");
            var req = UnityEngine.Networking.UnityWebRequest.Get(directFFMPEGDownloadUrl);
            var op = req.SendWebRequest();
            while(true) {
                UpdateProgress(0, op.progress);
                yield return null;
                if (op.isDone) break;
            }
            downloadingFFMPEG = false;
            if (req.isNetworkError || req.isHttpError) {
               StartCoroutine(CatastrophicFailure(req.error));
            }

            try { 
                new FileInfo(ffmpegPath).Directory.Create();
                File.WriteAllBytes(ffmpegPath, req.downloadHandler.data);
                haveFFMPEG = true;
            }
            catch(System.Exception e) {
                StartCoroutine(CatastrophicFailure(e.ToString()));
            }
        }
        
    }

    private IEnumerator CatastrophicFailure(string err) {
        Log("CatastrophicFailure " + err, true);
        ffmpegDLFailed = true;
        yield return new WaitForSecondsRealtime(10f);
        Destroy(this);
        yield break;
    }

    public Texture2D copytotex = null;
    public Texture2D texFromSmallerMip = null;

    public bool readPixelsInsteadOfRenderTexturing = false;
    public bool keepRefsToTextures = true;
    public bool keepRefsToRenderTexturesInstead = true;
    public bool fillRenderTextureWithOnRender = true;
    public bool grabPreviousFrameByNewCameraHack = true;

    int capturesThisSecCounter = 0;
    int capturesCounterForSecond = -1;
    int capturesLastSecond = 0;

    private IEnumerator CaptureOne()
    {
        capturesThisSecCounter++;

        Texture2D retainTex = null;
        RenderTexture retainRenderTex = null;

        if(readPixelsInsteadOfRenderTexturing) { 
            if (copytotex == null || copytotex.width != Screen.width || copytotex.height != Screen.height) {
                copytotex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, true);
            }

            copytotex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, true);

            //int halvingMod = 1;

            //screen 1920 and targ 960 = halvingmod is 2
            //mip level 1 gives 960, tex width is 1920/2 = 960 (WORKS)
            //Screen 1920 and targ 480 = halvingmod is 4
            //mip level 3 gives, err

            //256 mip levels are:
            //0: 256
            //1: 128
            //2: 64

            //how to calc: 256 / mip^2 = 2^2 = 4 = 256/4 = 64

            //int mipMod = (Screen.width / targetApproxVideoWidth) - 1;
            int portionMod = (Screen.width / targetApproxVideoWidth);

            int mipMod = Mathf.RoundToInt(Mathf.Sqrt(portionMod));


            //mipPixels = copytotex.GetPixels32(mipMod);


            int halvingMod = (int)Mathf.Pow(2, mipMod);
            //halvingMod = (int)Mathf.Pow(halvingMod, 2);

            if (texFromSmallerMip == null || texFromSmallerMip.width != Screen.width / halvingMod || texFromSmallerMip.height != Screen.height / halvingMod) {
                texFromSmallerMip = new Texture2D(Screen.width / halvingMod, Screen.height / halvingMod, TextureFormat.RGBA32, false);
            }

            Texture2D putToTex = null;

            if (keepRefsToTextures) {
                retainTex = GetCopyPooledRetainedTex(texFromSmallerMip);
                putToTex = retainTex;
            }
            else putToTex = texFromSmallerMip;

            //texFromSmallerMip.SetPixels32(mipPixels);
            UnityEngine.Graphics.CopyTexture(copytotex, 0, mipMod, putToTex, 0, 0);
        }

        else {
            if(singularRenderTex == null || singularRenderTex.width != targetApproxVideoWidth) {
                var aspect = (float)Screen.width / (float)Screen.height;
                var height = Mathf.RoundToInt(targetApproxVideoWidth / aspect);
                if (height % 2 != 0) height--;

                singularRenderTex = new RenderTexture(targetApproxVideoWidth, height, 16,RenderTextureFormat.ARGB32);
            }

            RenderTexture putRenderingToTex = null;
            if (keepRefsToRenderTexturesInstead) {
                retainRenderTex = GetCopyPooledRetainedRenderTex(singularRenderTex);
                putRenderingToTex = retainRenderTex;
            }
            else putRenderingToTex = singularRenderTex;

            if(fillRenderTextureWithOnRender) {
                Debug.Assert(renderTexGrabber != null, "!(renderTexGrabber != null");
                renderTexGrabber.outputRenderTex = putRenderingToTex;
                renderTexGrabber.blitScale = new Vector2(1f, 1f);
                yield return null; //wait for a frame for it to render :DDD how does one timings
            }
            else { 
                Camera.main.targetTexture = putRenderingToTex;
                Camera.main.Render();
                Camera.main.targetTexture = null;
            }

            if (!keepRefsToRenderTexturesInstead) { 
                if (texFromSmallerMip == null || texFromSmallerMip.width != singularRenderTex.width || texFromSmallerMip.height != singularRenderTex.height) {
                    texFromSmallerMip = new Texture2D(singularRenderTex.width, singularRenderTex.height, TextureFormat.ARGB32, false);
                }

                //Graphics.CopyTexture(renderTex, texFromSmallerMip); //copies this on gpu side but that's pretty much useless as it won't be on the cpu
                //texFromSmallerMip.Apply(); //goes only cpu->gpu direction so useless
                //texFromSmallerMip.IncrementUpdateCount();

                Texture2D putRTToTex = null;

                if (keepRefsToTextures) {
                    retainTex = GetCopyPooledRetainedTex(texFromSmallerMip);
                    putRTToTex = retainTex;
                }
                else putRTToTex = texFromSmallerMip;

                RenderTexture.active = singularRenderTex;
                putRTToTex.ReadPixels(new Rect(0, 0, singularRenderTex.width, singularRenderTex.height), 0, 0, false);
                RenderTexture.active = null;

                //putRTToTex.Apply(); //REMOVE, FOR DEBUG

            }
        }


        var capturedFrame = new CapturedFrame();

        if(keepRefsToRenderTexturesInstead) {
            capturedFrame.width = retainRenderTex.width;
            capturedFrame.height = retainRenderTex.height;

            capturedFrame.retainedRenderTex = retainRenderTex;
        }
        else {
            capturedFrame.width = texFromSmallerMip.width;
            capturedFrame.height = texFromSmallerMip.height;
            if (keepRefsToTextures) {
                //var retainedtex = GetCopyPooledRetainedTex(texFromSmallerMip);
                capturedFrame.retainedTexture = retainTex;
            }
            else capturedFrame.rawData = texFromSmallerMip.GetRawTextureData();
        }

        capturedFramesBuffer.Add(capturedFrame);



        //int removedCount = capturedFramesBuffer.RemoveAll(x => x.width != capturedFrame.width || x.height != capturedFrame.height);
        //if(removedCount > 0) {
        //    print("removed " + removedCount + " frames because width/height is different from latest");
        //}

        var wrongFrames = capturedFramesBuffer.Where(x => x.width != capturedFrame.width || x.height != capturedFrame.height).ToList();

        foreach (var item in wrongFrames) {
            RemoveFrameFromBuffer(item);
        }

        if(wrongFrames.Count > 0) {
            print("removed " + wrongFrames.Count + " frames because width/height is different from latest");
        }

        yield return null;
    }

    private void RemoveFrameFromBuffer(CapturedFrame item) {
        capturedFramesBuffer.Remove(item);
        if(item.retainedTexture != null)GiveTexToPool(item.retainedTexture);
        if (item.retainedRenderTex != null) GiveTexToRenderTexPool(item.retainedRenderTex);
    }

    public List<Texture2D> pooledRetainTexturesNotInUse = new List<Texture2D>();
    public List<Texture2D> pooledRetainTexturesCurrentlyInUse = new List<Texture2D>();

    private Texture2D GetCopyPooledRetainedTex(Texture2D referenceTexToCopyFrom) {

        var nonConforming = pooledRetainTexturesNotInUse.Where(x => x.width != referenceTexToCopyFrom.width || x.height != referenceTexToCopyFrom.height).ToList();
        foreach (var item in nonConforming) {
            print("wrong size texture in pool, destroying:"+item);
            pooledRetainTexturesNotInUse.Remove(item);
            DestroyImmediate(item);
        }

        if(pooledRetainTexturesNotInUse.Count == 0) {
            pooledRetainTexturesNotInUse.Add(new Texture2D(referenceTexToCopyFrom.width, referenceTexToCopyFrom.height, texFromSmallerMip.format, false));
        }
        var retainedTex = pooledRetainTexturesNotInUse[0];
        pooledRetainTexturesNotInUse.RemoveAt(0);

        //Graphics.CopyTexture(referenceTexToCopyFrom, retainedTex);
        pooledRetainTexturesCurrentlyInUse.Add(retainedTex);
        return retainedTex;
    }

    private void GiveTexToPool(Texture2D retainedTexture) {
        pooledRetainTexturesCurrentlyInUse.Remove(retainedTexture);
        pooledRetainTexturesNotInUse.Add(retainedTexture);
    }

    public List<RenderTexture> pooledRetainRenderTexturesNotInUse = new List<RenderTexture>();
    public List<RenderTexture> pooledRetainRenderTexturesCurrentlyInUse = new List<RenderTexture>();

    private RenderTexture GetCopyPooledRetainedRenderTex(RenderTexture referenceRenderTex) {
        var nonConforming = pooledRetainRenderTexturesNotInUse.Where(x => x.width != referenceRenderTex.width || x.height != referenceRenderTex.height).ToList();
        foreach (var item in nonConforming) {
            print("wrong size texture in rendertex pool, destroying:" + item);
            pooledRetainRenderTexturesNotInUse.Remove(item);
            DestroyImmediate(item);
        }

        if (pooledRetainRenderTexturesNotInUse.Count == 0) {
            pooledRetainRenderTexturesNotInUse.Add(new RenderTexture(referenceRenderTex.width, referenceRenderTex.height, referenceRenderTex.depth, referenceRenderTex.format));
            pooledRetainRenderTexturesNotInUse.Last().antiAliasing = rtAntiAliasingAmount;
        }
        var retainedTex = pooledRetainRenderTexturesNotInUse[0];
        pooledRetainRenderTexturesNotInUse.RemoveAt(0);

        //Graphics.CopyTexture(referenceTexToCopyFrom, retainedTex);
        pooledRetainRenderTexturesCurrentlyInUse.Add(retainedTex);
        return retainedTex;
    }

    private void GiveTexToRenderTexPool(RenderTexture retainedRenderTexture) {
        pooledRetainRenderTexturesCurrentlyInUse.Remove(retainedRenderTexture);
        pooledRetainRenderTexturesNotInUse.Add(retainedRenderTexture);
    }


    public bool hashtagsDebug = false;


#if !IS_TOT_3D_GAME
    void OnGUI() {
        ManuallyCalledOnGUI();
    }
#endif

    bool showPassiveStats = true;

    public void ManuallyCalledOnGUI() {

        if (timeSpentStopWatch == null) return;

        if (!haveFFMPEG) {
            if(ffmpegDLFailed) {
                GUILayout.Label("PoorMansVideoCapture: FAILED downloading FFMPEG!");
                return;
            }

            if(downloadingFFMPEG) { 
                GUILayout.Label("PoorMansVideoCapture: downloading FFMPEG");
                GUILayout.Label(GetASCIIProgressBar(progressInPhase, 100));
            }
            return;
        }

        var timeSinceProcessingDone = Time.realtimeSinceStartup - lastProcessingDoneTime;
        if (processingInProgress || timeSinceProcessingDone < 10f) {
            GUILayout.Label("PoorMansVideoCapture PROCESSING:");
            GUILayout.Label("PHASE:" + progressPhase);
            GUILayout.Label(GetASCIIProgressBar(progressInPhase, 100));
            float maxProg = 2;
            GUILayout.Label(GetASCIIProgressBar((progressInPhase + progressPhase) / maxProg, 100));

            if (!processingInProgress) {
                var totalTime = lastImageOutputtingTook + lastFFmpegTook;
                GUILayout.Label("PROCESSING DONE in " + totalTime + " s " + "(image output:" + lastImageOutputtingTook.TotalSeconds + "s, ffmpeg:" + lastFFmpegTook.TotalSeconds + "s)");
            }
        }

        if (capturingOn) {

            if(showPassiveStats) { 
                GUILayout.Label("PoorMansVideoCapture: capturing flashback");

                long memUseByTextures = 0;
                long memoryUsedByaSingleFrame = 0;
                bool guessedMemoryUse = false;
            
                var first = capturedFramesBuffer.FirstOrDefault();
                if(first != null) {
                    if(first.retainedTexture) memoryUsedByaSingleFrame = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(first.retainedTexture);
                    if (first.retainedRenderTex) memoryUsedByaSingleFrame = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(first.retainedRenderTex);

                    if (memoryUsedByaSingleFrame == 0) {
                        memoryUsedByaSingleFrame = 537600;
                        guessedMemoryUse = true;
                    }

                    memUseByTextures = memoryUsedByaSingleFrame * capturedFramesBuffer.Count;
                }
                string extraStr = guessedMemoryUse ? "(VERY MAYBE, not devbuild)" : "";
                GUILayout.Label("Memory used "+extraStr+" by flashback buffer:"+ByteLenghtToHumanReadable(memUseByTextures));
                GUILayout.Label("flashback time: "+ Mathf.Round((float)capturedFramesBuffer.Count / targetFPS)+" sec / "+flashbackCaptureSecondsCount+" sec" + "  captures last sec:"+ capturesLastSecond);
                GUILayout.Label("Press Shift+I+P to toggle this text");
                GUILayout.Label("Press Shift+U+P to inspect old frames (Shift+U+L to pause capture)");
                GUILayout.Label("Press Shift+I+K to disable this entirely");

                if (hashtagsDebug) { 
                    var str = new string('#', Time.frameCount);
                    GUILayout.Label(str);
                }
            }
        }

        if(processingInProgress && showPassiveStats) {
            var relevantIndex = progressPhase == 0 ? lastimagesSavedCount : ffmpegFramesProcessed;
            var relevantTex = capturedFramesBuffer[relevantIndex].retainedRenderTex;
            GUILayout.Label(relevantTex);
        }


        if(inspectingOldFrames) {            
            GUILayout.Label("Left / Right to scroll frames, shift to go faster");
            var inspectSpeedMod = Input.GetKey(KeyCode.LeftShift) ? targetFPS * 3f : targetFPS;
            if (onGuisCountPerFrame != 0) inspectSpeedMod = 0;
            inspectingOldFramesIndex += (Input.GetAxis("Horizontal") * inspectSpeedMod * Time.smoothDeltaTime);

            inspectingOldFramesIndex = Mathf.Clamp(inspectingOldFramesIndex, 0, capturedFramesBuffer.Count - 1);
            var relevantFrame = capturedFramesBuffer[Mathf.RoundToInt(inspectingOldFramesIndex)];
            var relevantTex = relevantFrame.retainedRenderTex;

            var shownWidth = Mathf.Clamp(relevantFrame.width, 1f, 700f);
            //var shownHeight = relevantFrame.height / ((float)relevantFrame.width / (float)relevantFrame.height);

            /*var ogColor = GUI.color;
            GUI.color = Color.white;
            GUI.backgroundColor = Color.white;            
            GUI.contentColor = Color.white;
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.white;*/
            GUILayout.Label(relevantTex,/*style,*/GUILayout.MaxWidth(shownWidth)/*,GUILayout.MaxHeight(shownHeight)*/);
            //GUI.color = ogColor;

            /*style.normal.background = Texture2D.whiteTexture;
            GUI.Label(new Rect(new Vector2(0f, 0f), Vector2.one * 300), relevantTex,style);*/

            inspectingOldFramesIndex = GUILayout.HorizontalSlider(inspectingOldFramesIndex, 0f, capturedFramesBuffer.Count - 1f, GUILayout.Width(relevantTex.width));

            GUILayout.BeginHorizontal();
            List<CapturedFrame> framesToRemove = new List<CapturedFrame>();
            var frameBeforeCut = relevantFrame;
            if (GUILayout.Button("Cut out frames to left")) {
                for (int i = 0; i < capturedFramesBuffer.Count; i++) {
                    if(Mathf.FloorToInt(inspectingOldFramesIndex) > i) {
                        framesToRemove.Add(capturedFramesBuffer[i]);                        
                    }
                }
            }
            if (GUILayout.Button("Cut out frames to right")) {
                for (int i = 0; i < capturedFramesBuffer.Count; i++) {
                    if (Mathf.CeilToInt(inspectingOldFramesIndex) < i) {
                        framesToRemove.Add(capturedFramesBuffer[i]);
                    }
                }
            }
            GUILayout.EndHorizontal();
            foreach (var item in framesToRemove) {
                RemoveFrameFromBuffer(item);
            }
            if(framesToRemove.Count > 0) {
                Log("Snipped " + framesToRemove.Count + " frames out");
                inspectingOldFramesIndex = capturedFramesBuffer.IndexOf(frameBeforeCut);
            }

            if (GUILayout.Button("Capture")) {
                SaveFlashbackRecording();
            }

            if(Time.realtimeSinceStartup - timeLastRemovedFrameBecauseBufferLimit < 0.2f) {
                var ogColor = GUI.color;
                GUI.color = (Time.time % 0.1f < 0.05f ? UnityEngine.Color.black : UnityEngine.Color.yellow);
                GUILayout.Label("BUFFER OVERWRITING ITSELF BECAUSE LIMIT");
                GUI.color = ogColor;
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label("Target fps:"+targetFPS);
            targetFPS = GUILayout.HorizontalSlider(targetFPS, 1f, 120f);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Resolution width:"+targetApproxVideoWidth);
            targetApproxVideoWidth = (int)GUILayout.HorizontalSlider(targetApproxVideoWidth, 128f, 3840f);
            //targetApproxVideoWidth = Mathf.Round(targetApproxVideoWidth);
            if (targetApproxVideoWidth % 2 != 0) targetApproxVideoWidth--;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Flashback lenght:"+flashbackCaptureSecondsCount);
            flashbackCaptureSecondsCount = GUILayout.HorizontalSlider(flashbackCaptureSecondsCount, 0.1f, 120f);
            GUILayout.EndHorizontal();
        }
        onGuisCountPerFrame++;
    }

    bool inspectingOldFrames = false;
    float inspectingOldFramesIndex = 0;

    private string GetASCIIProgressBar(float progressGues, int widthInChars) {
        var charsCount = widthInChars;
        //charsCount = 20;

        var donesCount = (int)System.Math.Round(progressGues * charsCount);
        var emptysCount = charsCount - donesCount;
        if (emptysCount < 0) emptysCount = 0;

        return "[" + new string('0', donesCount) + new string('_', emptysCount) + "]";

        //var lines = new String('-', Console.WindowWidth - 10);
    }    

    public Camera overrideGrabbedCamera;

    Camera GetGrabbedCamera() {
        if (overrideGrabbedCamera != null) return overrideGrabbedCamera;        

        if(grabPreviousFrameByNewCameraHack) {
            if(previousFrameGrabberDummyCamera == null) {
                previousFrameGrabberDummyCamera = new GameObject("previousFrameGrabberDummyCamera").AddComponent<Camera>();
                previousFrameGrabberDummyCamera.transform.position = Vector3.up * 15000;
                previousFrameGrabberDummyCamera.cullingMask = 0;
                previousFrameGrabberDummyCamera.farClipPlane = 1f; //have to have this somewhere or the camera refuses to render
                previousFrameGrabberDummyCamera.depth = -100f;
                previousFrameGrabberDummyCamera.clearFlags = CameraClearFlags.Nothing;
                previousFrameGrabberDummyCamera.useOcclusionCulling = false;
                previousFrameGrabberDummyCamera.orthographic = true;
                //DontDestroyOnLoad(previousFrameGrabberDummyCamera.gameObject);
                previousFrameGrabberDummyCamera.hideFlags = HideFlags.DontSave;
            }
            return previousFrameGrabberDummyCamera;
        }

        return Camera.main;
    }
    Camera previousFrameGrabberDummyCamera = null;

    CameraRenderTexGrabber renderTexGrabber = null;

    void OnDisable() {
        if (previousFrameGrabberDummyCamera != null) previousFrameGrabberDummyCamera.enabled = false;
    }
    void OnEnable() {
        if (timeSpentStopWatch == null)
            timeSpentStopWatch = System.Diagnostics.Stopwatch.StartNew(); //assemblylreload

        if (previousFrameGrabberDummyCamera != null) previousFrameGrabberDummyCamera.enabled = true;
    }
    void OnDestroy() {
        foreach (var item in pooledRetainTexturesNotInUse) {
            DestroyImmediate(item);
        }

        foreach (var item in pooledRetainRenderTexturesCurrentlyInUse) {
            if (item == null) continue;
            DestroyImmediate(item);
        }
        foreach (var item in pooledRetainRenderTexturesNotInUse) {
            if (item == null) continue;
            DestroyImmediate(item);
        }


        if (previousFrameGrabberDummyCamera != null) DestroyImmediate(previousFrameGrabberDummyCamera.gameObject);
    }

    void Update() {

        while(needingToRunOnMainThread.Count != 0) {
            var act = needingToRunOnMainThread[0];
            try {
                act();
            }
            catch(System.Exception e) {
                Log("needingToRunOnMainThread action crash:" + e, true);
            }
            needingToRunOnMainThread.RemoveAt(0);
        }

        bool shouldHaveRenderTexGrabber = fillRenderTextureWithOnRender;
        bool haveGrabber = renderTexGrabber != null;

        if (shouldHaveRenderTexGrabber != haveGrabber) {
            if (shouldHaveRenderTexGrabber) {
                if (GetGrabbedCamera() != null) {
                    Log("Adding CameraRenderTexGrabber to a camera:" + GetGrabbedCamera());
                    renderTexGrabber = GetGrabbedCamera().gameObject.AddComponent<CameraRenderTexGrabber>();
                    //if (grabPreviousFrameByNewCameraHack) renderTexGrabber.doBlitNormalImage = false;
                }
            }
            else {
                Log("Destroying CameraRenderTexGrabber as its not wanted");
                DestroyImmediate(renderTexGrabber);
                if (previousFrameGrabberDummyCamera != null) Destroy(previousFrameGrabberDummyCamera.gameObject);
            }
        }


        var flashbackFrameCountTarget = Mathf.RoundToInt(flashbackCaptureSecondsCount * targetFPS);
        var extraFramesCount = capturedFramesBuffer.Count - flashbackFrameCountTarget;

        for (int i = 0; i < extraFramesCount; i++) {
            RemoveFrameFromBuffer(capturedFramesBuffer[0]);
        }
        if (extraFramesCount > 0) timeLastRemovedFrameBecauseBufferLimit = Time.realtimeSinceStartup;

        if (Input.GetKeyDown(KeyCode.Alpha0)) {
            SaveFlashbackRecording();
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.I) && Input.GetKeyDown(KeyCode.P)) {
            showPassiveStats = !showPassiveStats;
            PlayerPrefs.SetInt("PoorMansVideoPassiveUIVisible", showPassiveStats ? 1 : 0);
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.I) && Input.GetKeyDown(KeyCode.K)) {
            Log("Key combination pressed - destroying self");
            Destroy(gameObject);
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.U) && Input.GetKeyDown(KeyCode.P)) {
            inspectingOldFrames = !inspectingOldFrames;
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.U) && Input.GetKeyDown(KeyCode.L)) {
            if(!processingInProgress) {
                capturingOn = !capturingOn;
            }
        }

        onGuisCountPerFrame = 0;
    }

    /*private void SaveFlashbackRecording() {
        SaveFlashbackRecording(null, "");
    }*/

    public void SaveFlashbackRecording(System.Action<string, bool> onVideoSavingComplete = null, string saveToPath = "") {        

        Log("SaveFlashbackRecording requested");

        if(processingInProgress) {
            Log("SaveFlashbackRecording failed: Already in progress!");
            if (onVideoSavingComplete != null) {
                onVideoSavingComplete("SaveFlashbackRecording failed: Already in progress!", false);
            }
            return;
        }

        if(!haveFFMPEG) {
            Log("SaveFlashbackRecording: Don't have ffmpeg (auto-download failed?)");
            if (onVideoSavingComplete != null) {
                onVideoSavingComplete("SaveFlashbackRecording: Don't have ffmpeg (auto-download failed?)", false);
            }
            return;
        }

        capturingOn = false;

        this.onVideoSavingComplete = onVideoSavingComplete;

        StartCoroutine(BufferSaving(saveToPath));
    }

    Texture2D texOnlyUsedByLastRTToPng = null;

    #if USE_EXTERNAL_IMAGESAVING
    bool textureResolvingDone = false;
    #endif

    public List<KeyValuePair<int, CapturedFrame>> rawFramesToWrite = new List<KeyValuePair<int, CapturedFrame>>();

    public bool UseExternalImageSaving {
        get {
            #if USE_EXTERNAL_IMAGESAVING
            return true;
            #endif
            return false;
        }
    }

    private IEnumerator BufferSaving(string saveToPath = "") {

        var bufferSavingTimer = System.Diagnostics.Stopwatch.StartNew();

        processingInProgress = true;

        #if USE_EXTERNAL_IMAGESAVING
        textureResolvingDone = false;
        #endif
                
        var capName = "cap" + new System.Random().Next();
        var tempFolder = new DirectoryInfo(applicationDotTemporaryCachePath + "/PoorMansVideoCaptureTemp/"+capName);
        Log("make temp folder to "+tempFolder.FullName);
        if (!tempFolder.Exists) tempFolder.Create();


        #if USE_EXTERNAL_IMAGESAVING
        Thread writingToDiskThread = new Thread( () => WriteTransferredRawTexDatasToDiskThreaded(tempFolder) );
        writingToDiskThread.Start();
        #endif

        Log("write png frames to folder");

        float timelastWaited = Time.realtimeSinceStartup;
        for (int i = 0; i < capturedFramesBuffer.Count; i++) {

            #if !USE_EXTERNAL_IMAGESAVING
            UpdateProgress(0, (float)i / (float)capturedFramesBuffer.Count);
            #else
            var timer = System.Diagnostics.Stopwatch.StartNew();
            #endif


            Texture2D pngFromTexture = null;
            if(capturedFramesBuffer[i].retainedTexture != null) {
                pngFromTexture = capturedFramesBuffer[i].retainedTexture;
            }
            else if(capturedFramesBuffer[i].retainedRenderTex != null) {
                if(texOnlyUsedByLastRTToPng == null || texOnlyUsedByLastRTToPng.width != capturedFramesBuffer[i].width || texOnlyUsedByLastRTToPng.height != capturedFramesBuffer[i].height) {
                    texOnlyUsedByLastRTToPng = new Texture2D(capturedFramesBuffer[i].width, capturedFramesBuffer[i].height, TextureFormat.RGB24, false);
                }
                //yield return new WaitForEndOfFrame();

                RenderTexture.active = capturedFramesBuffer[i].retainedRenderTex;
                texOnlyUsedByLastRTToPng.ReadPixels(new Rect(0, 0, texOnlyUsedByLastRTToPng.width, texOnlyUsedByLastRTToPng.height), 0, 0, false);
                RenderTexture.active = null;

                pngFromTexture = texOnlyUsedByLastRTToPng;
            }
            else {
                texFromSmallerMip.LoadRawTextureData(capturedFramesBuffer[i].rawData);
                pngFromTexture = texFromSmallerMip;
            }

#if USE_EXTERNAL_IMAGESAVING
            capturedFramesBuffer[i].rawData = pngFromTexture.GetRawTextureData();
            rawFramesToWrite.Add( new KeyValuePair<int, CapturedFrame>(i, capturedFramesBuffer[i]));            
            Log("Resolving prog:" + i + " resolve took " + GetNiceTimerMSString(timer));
            //rawFramesToWrite.Add(new KeyValuePair<int, byte[]>(i, rawdata));
#else
            var fileBytes = pngFromTexture.EncodeToJPG(95);
            string imageFilePath = tempFolder + "/" +"frame"+ i.ToString("D10")+ImageFileExtension;
            File.WriteAllBytes(imageFilePath, fileBytes);
#endif

            float timeWorkedThisFrame = Time.realtimeSinceStartup - timelastWaited;
            if (timeWorkedThisFrame > 0.033f) {
                yield return null;
                timelastWaited = Time.realtimeSinceStartup;
            }

            lastimagesSavedCount = i;
        }        

        lastImageOutputtingTook = bufferSavingTimer.Elapsed;

#if USE_EXTERNAL_IMAGESAVING
        textureResolvingDone = true;
        while(writingToDiskThread.IsAlive) {
            Log("waiting for writing thread to get done and exit");
            yield return null;            
        }
#endif

        var ffmpegTimer = System.Diagnostics.Stopwatch.StartNew();

        var processingThread = new Thread(() => SaveCaptureBufferThreaded(tempFolder,saveToPath,capName));
        processingThread.Start();

        while (processingThread.IsAlive) yield return null;

        Log("Finished saving video (or failed)");
        Log("image output took "+lastImageOutputtingTook.TotalMilliseconds+" ms, FFMPEG took " + GetNiceTimerMSString(ffmpegTimer));

        lastFFmpegTook = ffmpegTimer.Elapsed;

        processingInProgress = false;
        lastProcessingDoneTime = Time.realtimeSinceStartup;

        var prevList = capturedFramesBuffer.ToList();
        foreach (var item in prevList) {
            RemoveFrameFromBuffer(item);
        }
        capturingOn = true;
    }

    public string ImageFileExtension {
        get {
            return ".jpg";
        }
    }

#if USE_EXTERNAL_IMAGESAVING
    private void WriteTransferredRawTexDatasToDiskThreaded(DirectoryInfo imagesOutputFolder) {
        while(true) {

            var info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().Single(x => x.FormatID == System.Drawing.Imaging.ImageFormat.Jpeg.Guid);
            var encoder = System.Drawing.Imaging.Encoder.Quality;
            var parms = new System.Drawing.Imaging.EncoderParameters(1);
            parms.Param[0] = new System.Drawing.Imaging.EncoderParameter(encoder, 90L);

            while (rawFramesToWrite.Count != 0) {
                var timer = System.Diagnostics.Stopwatch.StartNew();

                var rawframe = rawFramesToWrite[0];
                //string imageFilePath = imagesOutputFolder.FullName + "/" + "frame" + rawframe.Key.ToString("D10") + ".png";
                string imageFilePath = imagesOutputFolder.FullName + "/" + "frame" + rawframe.Key.ToString("D10") + ImageFileExtension;

                var bitmap = new Bitmap(rawframe.Value.width, rawframe.Value.height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                var bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                System.Runtime.InteropServices.Marshal.Copy(rawframe.Value.rawData, 0, bmpdata.Scan0, rawframe.Value.rawData.Length);

                bitmap.UnlockBits(bmpdata);
                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);                

                //var imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;                
                bitmap.Save(imageFilePath, info, parms);

                rawFramesToWrite.Remove(rawframe);
                Log("Writing prog:" + rawframe.Key + " write took " + GetNiceTimerMSString(timer));

                UpdateProgress(0, (float)rawframe.Key / (float)capturedFramesBuffer.Count);
            }

            if (textureResolvingDone) {
                Log("writing thread exiting");
                return;
            }

            Log("writing thread waiting for data");
            Thread.Sleep(1);
        }
    }
#endif

    private string GetNiceTimerMSString(System.Diagnostics.Stopwatch timer) {
        return System.Math.Round(((float)timer.ElapsedTicks / (float)System.Diagnostics.Stopwatch.Frequency) * 1000f, 2) + " ms";
    }

    int progressPhase;
    float progressInPhase;

    private void UpdateProgress(int phase, float phaseProg) {
        //print("UpdateProgress "+phase+"  "+phaseProg);

        progressPhase = phase;
        progressInPhase = phaseProg;
    }

    public string applicationDotTemporaryCachePath;
    public string applicationDotPersistentDataPath;

    public string ffmpegPath;

    public string ffmpegLogLevelString = "trace";

    public StringBuilder lastStdErrOutPuts;

    private void SaveCaptureBufferThreaded(DirectoryInfo captureFolder, string saveToPath, string capName) {
        try {
            //print("write raw frames to folder");
            //for (int i = 0; i < capturedFramesBuffer.Count; i++) {
            //    string rawfilepath = tempFolder + "/" + i;
            //    File.WriteAllBytes(rawfilepath, capturedFramesBuffer[i].rawData);
            //}
            Log("try make clip with ffmpeg");
                        

            if (saveToPath == "") {
                saveToPath = applicationDotPersistentDataPath + "/PoorMansVideoCapture" + capName;
            }
            if (!saveToPath.EndsWith(".mp4")) saveToPath = saveToPath + ".mp4";

            ffmpegProcess = new System.Diagnostics.Process();
            ffmpegProcess.StartInfo.FileName = ffmpegPath;
            //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            ffmpegProcess.StartInfo.CreateNoWindow = true;
            ffmpegProcess.StartInfo.WorkingDirectory = captureFolder.FullName;
            print("workingdir:" + ffmpegProcess.StartInfo.WorkingDirectory);

            ffmpegProcess.StartInfo.RedirectStandardOutput = true;
            ffmpegProcess.StartInfo.RedirectStandardError = true;
            ffmpegProcess.StartInfo.UseShellExecute = false;

            lastStdErrOutPuts = new StringBuilder();

            ffmpegProcess.OutputDataReceived += Process_OutputDataReceived;
            ffmpegProcess.ErrorDataReceived += Process_ErrorDataReceived;

            var arguments = "-framerate " + Mathf.RoundToInt(targetFPS) + " -i frame%010d"+ImageFileExtension+" -loglevel "+ffmpegLogLevelString+" -crf 25 \"" + saveToPath + "\"";
            print("arguments: " + arguments);
            ffmpegProcess.StartInfo.Arguments = arguments;

            //lastReceiveFromErrorTube = "";
            lastInfoWithMaybeProgress = "";
            ffmpegFramesProcessed = 0;

            ffmpegProcess.Start();
            ffmpegProcess.BeginErrorReadLine();
            ffmpegProcess.BeginOutputReadLine();

            do {
                Thread.Sleep(10);

                //var str = process.StandardOutput.ReadToEnd();
                //if (str.Length > 0) Debug.Log("FFMPEG:" + str);

                //str = process.StandardError.ReadToEnd();
                //if (str.Length > 0) Debug.LogError("FFMPEG ERROR:" + str);
                try {
                    if (lastInfoWithMaybeProgress != "") {
                        //Debug.Log(lastInfoWithMaybeProgress);

                        var afterframesplit = lastInfoWithMaybeProgress.Split(new[] { "frame=" }, System.StringSplitOptions.RemoveEmptyEntries).Last();
                        var beforeQP = afterframesplit.Split(new[] { "QP" }, System.StringSplitOptions.RemoveEmptyEntries).First();
                        var frameStr = beforeQP.Replace(" ", "");

                        //Debug.Log(frameStr);

                        //var frameStr = afterframesplit.Split(' ').First();
                        ffmpegFramesProcessed = int.Parse(frameStr);
                    }
                }
                catch (System.Exception err) {
                    Debug.LogWarning("fail parsing ffmpeg progress " + err+"\n\nTried to parse:\n\n"+lastInfoWithMaybeProgress);
                }

                UpdateProgress(1, (float)ffmpegFramesProcessed / (float)capturedFramesBuffer.Count);

            } while (!ffmpegProcess.HasExited);

            Log("ffmpeg done?");
            Thread.Sleep(100);
            Log("waited 100ms after ffmpeg maybe done");

            if (onVideoSavingComplete != null) {

               needingToRunOnMainThread.Add(() =>
               {
                   if (File.Exists(saveToPath)) {
                       onVideoSavingComplete(saveToPath, true);
                   }
                   else {
                       onVideoSavingComplete(saveToPath, false);
                   }
               });

            }

#if IS_TOT_3D_GAME
            var logDumpTimer = System.Diagnostics.Stopwatch.StartNew();
                        
            var hyperString = lastStdErrOutPuts.ToString();
            Log("logDump: sb tostring +" + GetNiceTimerMSString(logDumpTimer));

            bool maybeError = hyperString.Contains("Conversion failed");
            Log("logDump: containserror +" + GetNiceTimerMSString(logDumpTimer));

            var type = System.Type.GetType("RandomReportSender, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
            var meth = type.GetMethod("TryLogRandomAutoReport", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

            needingToRunOnMainThread.Add(() => {
                meth.Invoke(null, new object[] { hyperString, "poorMansffmpegRuns", "maybeErr:" + maybeError, false });
            });            

            //Log("logDump: reflectioncall randomreportsender " + GetNiceTimerMSString(logDumpTimer));
#endif


            //process.WaitForExit();
        }
        catch(System.Exception e) {
            Log("SaveCaptureBufferThreaded crashed! "+e,true);            
        }
    }

    /// <summary>
    /// string: path of video, bool: successful (false: failed, no video at path)
    /// </summary>
    public System.Action<string,bool> onVideoSavingComplete;

    int lastimagesSavedCount = 0;
    int ffmpegFramesProcessed = 0;

    public bool doDebugFFmpegOutPut = false;

    private void Process_ErrorDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e) {
        if(doDebugFFmpegOutPut)Log(sender + " " + e.Data,true);

        lastStdErrOutPuts.AppendLine(e.Data);

        if(e.Data.Contains("] frame=")) {
            lastInfoWithMaybeProgress = e.Data;
        }
        //lastReceiveFromErrorTube = e.Data;
    }

    private void Process_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e) {
        if (doDebugFFmpegOutPut) Log(sender + " " + e.Data);        
    }

    void Log(string inStr, bool error = false) {
        var str = "PoorMansVideoCapture [" + timeSpentStopWatch.Elapsed + "]" + inStr;
        //print(GetNiceTimerMSString(timeSpentStopWatch)+ timeSpentStopWatch.ElapsedMilliseconds);

        if (error) Debug.LogError(str);
        else Debug.Log(str);
    }

    void OnApplicationExit() {
        if(!ffmpegProcess.HasExited) {
            Log("OnApplicationExit and ffmpeg still in progress, might be goobenongs", true);
            //ffmpegProcess.Kill();
        }
    }

    public bool processingInProgress;
    private Color32[] mipPixels;

    private System.Diagnostics.Stopwatch timeSpentStopWatch;
    private System.Diagnostics.Process ffmpegProcess;
    private RenderTexture singularRenderTex;
    //private string lastReceiveFromErrorTube;
    private string lastInfoWithMaybeProgress = "";

    private bool haveFFMPEG = false;
    private bool ffmpegDLFailed = false;

    private List<System.Action> needingToRunOnMainThread = new List<System.Action>();
    private float lastProcessingDoneTime = -1000f;

    private System.TimeSpan lastFFmpegTook;
    private System.TimeSpan lastImageOutputtingTook;
    private float timeLastRemovedFrameBecauseBufferLimit;
    private int onGuisCountPerFrame;    

    public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits = false) {
        string suffix = "";
        long lenght;
        if (useBits) lenght = byteLenght * 8;
        else lenght = byteLenght;

        if (lenght < 1024) {
            if (useBits) suffix = " bits";
            else suffix = " B";
            return lenght + suffix;
        }
        else if (lenght < 1048576) {
            if (useBits) suffix = " kbits";
            else suffix = " kB";
            return (lenght / 1024) + suffix;
        }
        else {
            if (useBits) suffix = " Mbits";
            else suffix = " MB";
            return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
        }
    }
}
