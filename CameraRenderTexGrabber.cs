﻿using UnityEngine;

public class CameraRenderTexGrabber : MonoBehaviour
{
    public Vector2 blitScale;
    public RenderTexture outputRenderTex = null;
    public bool doBlitNormalImage = true;

    void OnRenderImage(RenderTexture src, RenderTexture dest) {
        if(doBlitNormalImage)Graphics.Blit(src, dest);

        Graphics.Blit(src, outputRenderTex, blitScale, Vector2.zero);        
    }
}