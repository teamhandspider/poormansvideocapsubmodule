What does PoorMansVideoCapture do?
-PoorMansVideoCapture is a system that allows video capture from any unity project without outside dependencies, although only works on x86 Windows.
Here's a rough comparison to a system like shadowplay:

Pro:Works with any GPU, and should work with any x86 windows machine.
Pro:Works without having to have any specific software / libraries already installed.
Pro:Very low performance hit while recording, similar to shadowplay (unless free VRAM / RAM supply is a problem)

Con:Resolution/framerate is very low in comparison.
Con:Has high VRAM requirements.

The main use case this system was created was to automatically create "flashback" (last ~60 seconds) recordings and attach them to bugreports.
PoorMansVideoCapture contains no bug reporting functionality of its own but it can easily be integrated to one.


How to use:

-Either have the PoorMansVideoCapture script in your scene or instantiate it during initialization.
-NOTE: You should call DontDestroyOnLoad on the gameobject with PoorMansVideoCapture or you may lose footage.
Instantiating during initialization is recommended to give the system time to download the FFMPEG executable, which takes a while and the system doesn't work without it.

-Configure the settings to your liking and desired VRAM usage.
PoorMansVideoCapture uses a lot of VRAM in its current form. It is recommended to keep the resolution, fps, and flashback buffer length low.
Default settings are:
	Width:480px
	Fps:15
	Flashback buffer length: 30 seconds.
	
	
DURING GAMEPLAY:

-To save the last seconds or so, call SaveFlashbackRecording(). Parameters:
	-onVideoSavingComplete: Pass in a method to be called after the video saving is done. NOTE: the saving can fail, then the boolean parameter in the action will be set to false.
	-saveToPath: Pass in a path where the video will be saved. If none is given, the path will auto-generate and you can get the path from the first parameter of onVideoSavingComplete.
	



Advanced description of operation:
PoorMansVideoCapture captures rendered frames in a method similar to how screen space effects work (OnRenderImage), but instead of only running the main Graphics.Blit, it also runs it to an additional RenderTexture.
These additional RenderTextures are pooled (basically a ring-buffer) and in effect contain the recorded video in VRAM until SaveFlashbackRecording() is called.

When SaveFlashbackRecording() is called, the system first retrieves the RenderTextures to the CPU side and writes them to JPG files in a temporary folder.
After that, and external ffmpeg.exe process is started with the folder and framerate as a parameters. ffmpeg then compiles these to an h264 mp4 video file.
The system optionally an user-supplied callback function when everything is done.


	
Version: 1.1.0
	First release. 
		-Cleaned up / refactored main API calls. 
		-Added some safety checks and error handling.
		-Updated FFMPEG download url to an independently hosted one.

Version: 1.0.0
	First versioned version. Was developed independently from, but was mostly used for generating bug reports for Thief of Thieves on PC.